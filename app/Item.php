<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Item extends Model
{

    protected $table = 'items';
    protected $fillable = ['nama_barang','foto','harga','deskripsi','stok','category_id','slug'];
    // protected $guarded = ['id'];

    public function category(){
    	return $this->belongsTo('App\Category');
    }
    public function order(){
    	return $this->hasOne('App\Order');
    }
    public function rates(){
    	return $this->hasMany('App\Rate');
    }
}
