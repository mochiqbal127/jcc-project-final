<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Rate extends Model
{

    protected $table = 'rates';
    protected $fillable = ['user_id','item_id','nilai','isi','order_id'];


    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function item(){
    	return $this->belongsTo('App\Item');
    }
    public function order(){
    	return $this->belongsTo('App\Order');
    }
}
