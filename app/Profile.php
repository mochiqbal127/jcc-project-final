<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Profile extends Model
{

    protected $table = 'profiles';
    protected $fillable = ['nama_lengkap','no_hp','provinsi','kota','alamat','foto','user_id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
