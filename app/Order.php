<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{

    protected $table = 'orders';
    // protected $fillable = ['items_id','users_id','pembayaran','ongkir','tanggal','status'];
    protected $guarded = ['id'];
    public function item(){
    	return $this->belongsTo('App\Item');
    }
    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function rate(){
    	return $this->hasOne('App\Rate');
    }
}
