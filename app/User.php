<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','isAdmin','isActive'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $users = [
        'email_verified_at' => 'datetime',
    ];
    public function profile(){
        return $this->hasOne('App\Profile');
    }
    public function orders(){
        return $this->hasMany('App\Order');
    }
    public function rates(){
        return $this->hasMany('App\Rate');
    }
    public function getRoleDisplayAttribute(){
        if (@$this->attributes['isAdmin'] == 1) return 'Admin';
        if (@$this->attributes['isAdmin'] == 0) return 'Customer';
        return '-';
    }
    public function getStatusDisplayAttribute(){
        if (@$this->attributes['isActive'] == 1) return 'Active';
        if (@$this->attributes['isActive'] == 0) return 'Nonactive';
        return '-';
    }
}
