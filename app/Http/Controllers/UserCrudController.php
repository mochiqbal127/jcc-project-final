<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Profile;

class UserCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('modul.user.index-user', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modul.user.create-user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name'      => 'required|min:8|max:45',
            'email'     => 'required|email:dns|unique:users',
            'password'  => 'required|min:8',
            'isAdmin'   => 'required'
        ]);
        $validatedData['password']  = Hash::make($validatedData['password']);
        $status = User::create($validatedData);

        if($status) return redirect('/dashboard/user')->with('success',"Swal.fire(
            'Success!',
            'Data Berhasil Disimpan',
            'success'
          )"
        );

        else return redirect('/dashboard/user')->with('error','Data gagal Disimpan!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $profile = Profile::where('user_id',$id)->first();
        if (!empty($profile)) {

            $provinces = OngkirController::get_province();
            $cities = OngkirController::city($profile->provinsi);
            return view('modul.user.show-user', compact('user','profile','provinces','cities'));
        }else{
            return view('modul.user.show-user', compact('user'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('modul.user.edit-user', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->email != $request->email) {
            if ($request->password != '') {
                $validatedData = $request->validate([
                    'name'      => 'required|min:8|max:45',
                    'email'     => 'required|email:dns|unique:users',
                    'password'  => 'required|min:8',
                    'isAdmin'   => 'required',
                    'isActive'   => 'required'
                ]);
                $validatedData['password']  = Hash::make($validatedData['password']);

                $status = $user->update($validatedData);
            }else{
                $validatedData = $request->validate([
                    'name'      => 'required|min:8|max:45',
                    'email'     => 'required|email:dns|unique:users',
                    'isAdmin'   => 'required',
                    'isActive'   => 'required'
                ]);
                $status = $user->update($validatedData);
            }
        }else{
            if ($request->password != '') {
                $validatedData = $request->validate([
                    'name'      => 'required|min:8|max:45',
                    'password'  => 'required|min:8',
                    'isAdmin'   => 'required',
                    'isActive'   => 'required'
                ]);
                $validatedData['password']  = Hash::make($validatedData['password']);

                $status = $user->update($validatedData);
            }else{
                $validatedData = $request->validate([
                    'name'      => 'required|min:8|max:45',
                    'isAdmin'   => 'required',
                    'isActive'   => 'required'
                ]);
                $status = $user->update($validatedData);
            }
        }


        if($status) return redirect('/dashboard/user')->with('success',"Swal.fire(
            'Success!',
            'Data Berhasil Diubah',
            'success'
          )"
        );

        else return redirect('/dashboard/user')->with('error','Data gagal Diubah!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->isActive == 0) {
            $user->isActive = 1;
        }else{
            $user->isActive = 0;
        }

        $status = $user->save();

        if($status) return redirect('/dashboard/user')->with('success',"Swal.fire(
            'Success!',
            'Data Status Berhasil Diubah',
            'success'
          )");
        else return redirect('/dashboard/user')->with('error','Data Status gagal Diubah!!');
    }
    public function createprofile($id)
    {
        $user = User::find($id);
        $provinces = OngkirController::get_province();
        return view('modul.profile.create-profile',compact('user','provinces'));
    }
}
