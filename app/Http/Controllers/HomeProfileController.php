<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\User;
use App\Rate;
use App\Item;
use App\Order;
use App\Http\Controllers\OngkirController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class HomeProfileController extends Controller
{
    public function index()
    {
        //return view('modul.profile.index-profile',['index-profile'=>Profile::findOrFail(auth()->user()->id)]);
        // $profile = Profile::all();
        $provinces = OngkirController::get_province();
        $cities = OngkirController::city(auth()->user()->profile->provinsi);
        // dd($cities);
        return view('home.profile.index-profile',compact('provinces','cities'));
        //$data['profile'] = Profile::join('users','users.id','=','profiles.user_id')
        //->select('profiles.*')->get();
        //return view ('modul.profile.index-profile', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $profile = Profile::all();
        $provinces = OngkirController::get_province();
        return view('home.profile.create-profile',compact('provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_lengkap'=>'required|max:45|string',
            'no_hp'=>'required|numeric',
            'provinsi'=>'required',
            'kota'=>'required',
            'alamat'=>'required',
            'foto'=>'required|mimes:jpg,png,jpeg',
        ]);

        $nama =  'foto' .  '-' .time() .'.'.$request->file('foto')->extension();
        $validatedData['user_id']=auth()->user()->id;

        $gambar = $request->foto;
        $new_gambar = time().' - '.$gambar->getClientOriginalName();
        $validatedData['foto'] = $new_gambar;
        // dd($validatedData);
        $status = Profile::create($validatedData);

        $gambar->move('img/profile/', $new_gambar);
        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //return view('modul.profile.edit-profile',compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        $provinces = OngkirController::get_province();
        return view('home.profile.edit-profile',compact('profile','provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama_lengkap'=>'required|max:45|string',
            'no_hp'=>'required|numeric',
            'provinsi'=>'required',
            'kota'=>'required',
            'alamat'=>'required',
            'foto'=>'mimes:jpg,png,jpeg',
            'user_id'=>'required',
        ]);
        $profile = Profile::find($id);
        if ($request->has('foto')) {
            $path ='img/profile/';
            File::delete($path,$profile->foto);
            $gambar = $request->foto;
            $new_gambar = time().' - '.$gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);
            $profile_data =[
                'nama_lengkap'=>$request->nama_lengkap,
                'no_hp'=>$request->no_hp,
                'provinsi'=>$request->provinsi,
                'kota'=>$request->kota,
                'alamat'=>$request->alamat,
                'foto'=>$new_gambar,
                'user_id'=>$request->user_id,

            ];

        } else {
            $profile_data =[
                'nama_lengkap'=>$request->nama_lengkap,
                'no_hp'=>$request->no_hp,
                'provinsi'=>$request->provinsi,
                'kota'=>$request->kota,
                'alamat'=>$request->alamat,
                'user_id'=>$request->user_id,
            ];
        }

        $profile_data['slug'] = implode('-', explode(' ', $profile_data['nama_lengkap']));
        $status = $profile->update($profile_data);

        if($status) return redirect('/profile')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Diupdate',
      'success'
    )");
        else return redirect('/profile')->with('error','Data gagal DiUpdate!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // File::delete('img/' . $profile->foto);
        // Rate::where('user_id',auth()->user()->id)->delete();
        // Item::where('user_id',auth()->user()->id)->delete();
        // Order::where('user_id',auth()->user()->id)->delete();
        User::find($id)->update(['isActive'=>0]);

        Auth::logout();
        // $profile->delete();
        return redirect('/');
    }
    public function account(){
        $user = User::find(auth()->user()->id);
        return view('home.profile.edit-akun',compact('user'));
    }
    public function account_save(Request $request)
    {
        $user = User::find(auth()->user()->id);
        if ($user->email != $request->email) {
            if ($request->password != '') {
                $validatedData = $request->validate([
                    'name'      => 'required|min:8|max:45',
                    'email'     => 'required|email:dns|unique:users',
                    'password'  => 'required|min:8',
                ]);
                $validatedData['password']  = Hash::make($validatedData['password']);

                $status = $user->update($validatedData);
            }else{
                $validatedData = $request->validate([
                    'name'      => 'required|min:8|max:45',
                    'email'     => 'required|email:dns|unique:users',
                ]);
                $status = $user->update($validatedData);
            }
        }else{
            if ($request->password != '') {
                $validatedData = $request->validate([
                    'name'      => 'required|min:8|max:45',
                    'password'  => 'required|min:8',
                ]);
                $validatedData['password']  = Hash::make($validatedData['password']);

                $status = $user->update($validatedData);
            }else{
                $validatedData = $request->validate([
                    'name'      => 'required|min:8|max:45',
                ]);
                $status = $user->update($validatedData);
            }
        }


        if($status) return redirect('/profile')->with('success',"Swal.fire(
            'Success!',
            'Data Berhasil Diubah',
            'success'
          )"
        );

        else return redirect('/profile')->with('error','Data gagal Diubah!!');
    }
}
