<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;


class MailController extends Controller
{
    public function sendEmail(){
    	$details = [
    		'title' => 'from shop',
    		'body' 	=> 'ini test email'
    	];
    	Mail::to(['arwanimaulana89@gmail.com','mochiqbal127@gmail.com','panjieka8@gmail.com'])->send(new SendMail($details));
    	return redirect('/');
    }
    public static function registerEmail($email){
    	$details = [
    		'title' => 'Register Berhasil',
    		'body' 	=> 'Selamat Datang '.$email.' Di JCC SHOP'
    	];
    	Mail::to($email)->send(new SendMail($details));
    	return redirect('/');
    }
    public static function newOrder($email){
    	$details = [
    		'title' => 'Order Berhasil',
    		'body' 	=> 'Order Berhasil dilakukan'
    	];
    	Mail::to(['arwanimaulana89@gmail.com','mochiqbal127@gmail.com','panjieka8@gmail.com',$email])->send(new SendMail($details));
    	return redirect('/');
    }
}
