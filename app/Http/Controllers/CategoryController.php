<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('modul.category.index-category', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modul.category.create-category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'nama' => 'required',
        ];
        $this->validate($request, $rules);
        $status = Category::create([
            'nama' => $request->nama,
        ]);
        if($status) return redirect('/dashboard/category')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Disimpan',
      'success'
    )");
        else return redirect('/dashboard/category')->with('error','Data gagal Disimpan!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $category = Category::find($category);
        return view('modul.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        // dd($category);
        return view('modul.category.edit-category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $rules=[
            'nama' => 'required',
        ];
        $this->validate($request, $rules);
        $category = Category::find($id);
        // $category->nama = $request->nama;
        $status = $category->update([
            'nama'=>$request->nama
        ]);
        if($status) return redirect('/dashboard/category')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Diupdate',
      'success'
    )");
        else return redirect('/dashboard/category')->with('error','Data gagal DiUpdate!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // $category = Category::find($category);
        $status = $category->delete();

        if($status) return redirect('/dashboard/category')->with('success',"Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )");
        else return redirect('/dashboard/category')->with('error','Data gagal DiHapus!!');
    }
}
