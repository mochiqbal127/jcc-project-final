<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\MailController;

class UserController extends Controller
{
    function loginView(){
        return view('auth.login');
    }
    function registerView() {
        return view('auth.register');
    }
    function register(Request $request){
        $validatedData = $request->validate([
            'name'      => 'required|min:8|max:45',
            'email'     => 'required|email:dns|unique:users',
            'password'  => 'required|min:8'
        ]);
        $validatedData['password']  = Hash::make($validatedData['password']);
        User::create($validatedData);
        MailController::registerEmail($validatedData['email']);
        return redirect('/signin');
    }
    public function login(Request $request){
        $validatedData = $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);
        if(Auth::attempt($validatedData)){
            $request->session()->regenerate();
            if (auth()->user()->isActive != 0) {
                if (auth()->user()->profile != null) {
                    return redirect()->intended('/');
                }else{
                    return redirect()->intended('/profile/create');
                }
            }else{
                Auth::logout();
                $request->session()->invalidate();
                $request->session()->regenerateToken();
                return back()->with('error','Akun anda dinonaktifkan, hubungi admin untuk mengaktifkan kembali akun anda!');
            }
        }
        return back()->with('error','Email atau Password anda salah!');

    }
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
    public function index(){
        Auth::user->id();
        return view('modul.profile.index-profile', compact('profile'));
      }

}
