<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\User;
use App\Rate;
use App\Item;
use App\Order;
use App\Http\Controllers\OngkirController;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('modul.profile.index-profile',['index-profile'=>Profile::findOrFail(auth()->user()->id)]);
        // $profile = Profile::all();
        $provinces = OngkirController::get_province();
        $cities = OngkirController::city(auth()->user()->profile->provinsi);
        // dd($cities);
        return view('modul.profile.index-profile',compact('provinces','cities'));
        //$data['profile'] = Profile::join('users','users.id','=','profiles.user_id')
        //->select('profiles.*')->get();
        //return view ('modul.profile.index-profile', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $profile = Profile::all();
        $provinces = OngkirController::get_province();
        return view('modul.profile.create-profile',compact('provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_lengkap'=>'required|max:45|string',
            'no_hp'=>'required|numeric',
            'provinsi'=>'required',
            'kota'=>'required',
            'alamat'=>'required',
            'foto'=>'required|mimes:jpg,png,jpeg',
        ]);

        $nama =  'foto' .  '-' .time() .'.'.$request->file('foto')->extension();
        $validatedData['user_id']=$request->user_id;

        $gambar = $request->foto;
        $new_gambar = time().' - '.$gambar->getClientOriginalName();
        $validatedData['foto'] = $new_gambar;
        // dd($validatedData);
        $status = Profile::create($validatedData);

        $gambar->move('img/profile/', $new_gambar);
        return redirect('/dashboard/user/'.$request->user_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //return view('modul.profile.edit-profile',compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $profile = Profile::find($id);
        $profile = Profile::where('id',$id)->first();
        $provinces = OngkirController::get_province();
        return view('modul.profile.edit-profile',compact('profile','provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama_lengkap'=>'required|max:45|string',
            'no_hp'=>'required|numeric',
            'provinsi'=>'required',
            'kota'=>'required',
            'alamat'=>'required',
            'foto'=>'mimes:jpg,png,jpeg',
            'user_id'=>'required',
        ]);
        // $profile = Profile::find($id);
        $profile = Profile::where('id',$id)->first();
        if ($request->has('foto')) {
            $path ='img/profile/';
            File::delete($path,$profile->foto);
            $gambar = $request->foto;
            $new_gambar = time().' - '.$gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);
            $profile_data =[
                'nama_lengkap'=>$request->nama_lengkap,
                'no_hp'=>$request->no_hp,
                'provinsi'=>$request->provinsi,
                'kota'=>$request->kota,
                'alamat'=>$request->alamat,
                'foto'=>$new_gambar,
                'user_id'=>$request->user_id,

            ];

        } else {
            $profile_data =[
                'nama_lengkap'=>$request->nama_lengkap,
                'no_hp'=>$request->no_hp,
                'provinsi'=>$request->provinsi,
                'kota'=>$request->kota,
                'alamat'=>$request->alamat,
                'user_id'=>$request->user_id,
            ];
        }

        $profile_data['slug'] = implode('-', explode(' ', $profile_data['nama_lengkap']));
        $status = $profile->update($profile_data);

        if($status) return redirect('/dashboard/user/'.$request->user_id)->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Diupdate',
      'success'
    )");
        else return redirect('/dashboard/profile')->with('error','Data gagal DiUpdate!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        // File::delete('img/' . $profile->foto);
        // Rate::where('user_id',auth()->user()->id)->delete();
        // Item::where('user_id',auth()->user()->id)->delete();
        // Order::where('user_id',auth()->user()->id)->delete();
        User::find($profile->user_id)->update(['isActive'=>0]);

        Auth::logout();
        Request::session()->invalidate();
        Request::session()->regenerateToken();
        // $profile->delete();
        return redirect('/');
    }
}
