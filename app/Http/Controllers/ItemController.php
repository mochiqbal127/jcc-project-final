<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use File;
Use App\Category;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::with('category')->get();
        // dd($items);
        return view('modul.item.index-item', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('modul.item.create-item',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'nama_barang' => 'required',
            'foto' => 'required|mimes:jpeg,jpg,png|max:2200',
            'harga' => 'required',
            'deskripsi' => 'required',
            'stok' => 'required|numeric',
            'category_id' => 'required',

        ];
        $validatedData = $this->validate($request, $rules);
        $gambar = $request->foto;
        $new_gambar = time().' - '.$gambar->getClientOriginalName();
        $validatedData['foto'] = $new_gambar;
        $validatedData['slug'] = implode('-', explode(' ', $validatedData['nama_barang']));
        // dd($validatedData);
        $status = Item::create($validatedData);

        $gambar->move('img/item/', $new_gambar);

        if($status) return redirect('/dashboard/item')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Disimpan',
      'success'
    )");
        else return redirect('/dashboard/item')->with('error','Data gagal Disimpan!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($item)
    {
        $item = Item::find($item);
        return view('modul.item.show-item', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        return view('modul.item.edit-item', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $rules=[
            'nama_barang' => 'required',
            'foto' => 'mimes:jpeg,jpg,png|max:2200',
            'harga' => 'required',
            'deskripsi' => 'required',
            'stok' => 'required',
            'category_id' => 'required',

        ];
        $this->validate($request, $rules);
        $item = Item::find($id);
        if ($request->has('foto')) {
            $path ='img/item/';
            File::delete($path,$item->foto);
            $gambar = $request->foto;
            $new_gambar = time().' - '.$gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);
            $item_data =[
                'nama_barang' => $request->nama_barang,
                'foto' => $new_gambar,
                'harga' => $request->harga,
                'deskripsi' => $request->deskripsi,
                'stok' => $request->stok,
                'category_id' => $request->category_id,


            ];

        } else {
            $item_data =[
                'nama_barang' => $request->nama_barang,
                'harga' => $request->harga,
                'deskripsi' => $request->deskripsi,
                'stok' => $request->stok,
                'category_id' => $request->category_id,

            ];
        }

        $item_data['slug'] = implode('-', explode(' ', $item_data['nama_barang']));
        $status = $item->update($item_data);

        if($status) return redirect('/dashboard/item')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Diupdate',
      'success'
    )");
        else return redirect('/dashboard/item')->with('error','Data gagal DiUpdate!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $status = $item->delete();
        $path ='img/item/';
        File::delete($path.$item->foto);
        if($status) return redirect('/dashboard/item')->with('success',"Swal.fire(
      'Success!',
      'Data Berhasil Dihapus',
      'success'
    )");
        else return redirect('/dashboard/item')->with('error','Data gagal DiHapus!!');
    }
}
