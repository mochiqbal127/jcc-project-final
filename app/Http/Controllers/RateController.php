<?php

namespace App\Http\Controllers;

use App\Rate;
use App\Item;
use App\Category;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul.rate.index-rate',['rates'=>Rate::with(['item','user'])->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('modul.rate.create-rate',['item'=>Item::find($id),'categories'=>Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $request->validate([
            'nilai'=>'required|numeric|min:1|max:5',
            'isi' => 'required'
        ]);
        Rate::create([
            'order_id'=>$request->id,
            'item_id'=>$id,
            'user_id'=>auth()->user()->id,
            'nilai'=>$request->nilai,
            'isi'=>$request->isi,
        ]);
        return redirect('/cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function show(Rate $rate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function edit(Rate $rate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rate $rate)
    {
        //
    }
    public function category($name){
    	$category_id = Category::firstWhere($id);
    	// dd($category_id->id);
    	return view('modul.rate.create-rate',[
            'categories'=>Category::all(),'items'=>Item::where('category_id',$category_id->id)->with('category')->get()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rate $rate)
    {
        $rt = Rate::find($rate->id);
        $rt->delete();

        return redirect('/dashboard/rate')->with('success',"Swal.fire(
            'Success!',
            'Komentar Berhasil dihapus!',
            'success'
          )");
    }
}
