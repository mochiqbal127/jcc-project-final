<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Item;
use App\Order;
use App\Rate;

use App\Http\Controllers\OngkirController;
use App\Http\Controllers\MailController;


class HomeController extends Controller
{
    public function index(){
    	return view('home.index',['categories'=>Category::all(),'items'=>Item::with('category')->get()]);
    }
    public function category($name){
    	$category_id = Category::firstWhere('nama',$name);
    	// dd($category_id->id); 
    	return view('home.index',['categories'=>Category::all(),'items'=>Item::where('category_id',$category_id->id)->with('category')->get()]);
    }
    public function item(Request $request){
    	return view('home.index',['categories'=>Category::all(),'items'=>Item::where('nama_barang','like','%'.$request->key.'%')->with('category')->get()]);
    }
    public function show($name){
        $item=Item::firstWhere('slug',$name);
        $rates=Rate::where('item_id',$item->id)->paginate(3);
        // dd($item);
        return view('home.product',['categories'=>Category::all(),'item'=>$item,'items'=>Item::where('category_id',$item->category_id)->with('category')->get(),'rates'=>$rates]);
    }
    public function COview(Request $request, $id){
        Order::find($id)->update([
            'jumlah_barang'=>$request->jumlah_barang
        ]);
        // dd(Order::find($id));
        return view('home.checkout',['categories'=>Category::all(),'order'=>Order::find($id)]);
    }
    public function update(Request $request, $id){
        Order::find($id)->update([
            'status_barang' => 'dipesan',
            'ongkir'        => $request->ongkir,
            'harga_barang'  => $request->harga_barang,
            'total'         => $request->total,
            'ekspedisi'     => 'jne'
        ]);
        MailController::newOrder(auth()->user()->email);
        return redirect('/cart');
    }
    public function dashboard(){
        return view('dashboard');
    }
    
    public function about(){
        return view ('home.about-us');
    }
}

