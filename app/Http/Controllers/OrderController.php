<?php

namespace App\Http\Controllers;

use App\Order;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\OngkirController;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexCustomer()
    {
        return view('home.cart',['categories'=>Category::all(),'orders'=>Order::where('user_id',auth()->user()->id)->with('item')->get()]);
    }
    public function index(){

        return view('modul.order.index-order',['orders'=>Order::with(['item','user'])->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return redirect('/cart');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        if(!auth()->user()->profile){
            return redirect('profile/create');
        }
        Order::create([
            'item_id'=>$request->item_id,
            'user_id'=>auth()->user()->id
        ]);
        return redirect('/cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('modul.order.index-order',['orders'=>Order::with(['item','user'])->get()]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return redirect('/cart');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        if($request->status_pembayaran == 'Belum Bayar'){
            $order->update([
                'pembayaran'=>$request->pembayaran,
                'status_pembayaran'=>$request->status_pembayaran
            ]);
        }else{
            $order->update([
                'pembayaran'=>$request->pembayaran,
                'status_pembayaran'=>$request->status_pembayaran,
                'status_barang'=>'selesai'
            ]);
        }
        return redirect('/dashboard/order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::find($id)->delete();
        return redirect('/cart');
    }
}
