<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OngkirController;
use App\Http\Controllers\RateController;
use App\Http\Controllers\MailController;

use App\Http\Middleware\Authenticate;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/mail', 			'MailController@sendEmail');

Route::get('/', 				'HomeController@index');
Route::post('/',				'HomeController@item');
Route::get('/category/{name}', 	'HomeController@category');
Route::get('/item/{name}',		'HomeController@show');
Route::get('/about', 			'HomeController@about');

Route::get('/get_city/{id}',	'OngkirController@get_city');

Route::post('/get_cost',	'OngkirController@get_cost');

Route::group(['middleware' => ['auth']], function () {

	// Route::post('/add/{id}', 				'OrderController@create');
	Route::delete('/cart/{id}',				'OrderController@destroy');
	Route::get('/cart',			 			'OrderController@indexCustomer');
	Route::post('/cart',			 		'OrderController@store');
	Route::post('/checkout/order_id/{id}', 	'HomeController@COview');
	Route::put('/checkout/order_id/{id}', 	'HomeController@update');
	Route::get('/rate/create/item_{id}',	'RateController@create');
	Route::post('/rate/create/item_{id}',	'RateController@store');



	Route::post('/logout', 					'UserController@logout');
	Route::get('/logout', 					'UserController@logout');

	Route::group(['middleware' => ['isAdmin']], function () {
    Route::get('/dashboard', 				'HomeController@dashboard');
	Route::resource('/dashboard/rate', 		'RateController');
    Route::resource('/dashboard/category', 	'CategoryController');
    Route::resource('/dashboard/item', 		'ItemController');
	Route::resource('/dashboard/order',	 	'OrderController');
	Route::resource('/dashboard/profile', 	'ProfileController');
	Route::resource('/dashboard/user', 		'UserCrudController');
	Route::get('/dashboard/profile/create/{id}', 		'UserCrudController@createprofile');
	});
    Route::resource('/profile', 		'HomeProfileController');
    Route::get('/akun','HomeProfileController@account');
    Route::put('/akun','HomeProfileController@account_save');
});


// Login, Register, Logout
Route::group(['middleware' => ['guest']], function () {
	Route::get('/signin', 	'UserController@loginView')->name('login');
	Route::post('/signin', 	'UserController@login');

	Route::get('/signup', 	'UserController@registerView');
	Route::post('/signup',	'UserController@register');

});



