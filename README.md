# Final Project

# Kelompok 16

# Anggota Kelompok

-   Muhamad Arwani Maulana (arwanimaulana89@gmail.com) ,
    Pengerjaan :

1. Auth Login
2. Register
3. Logout
4. Templating Home
5. Eloquent ORM
6. Modul Order
7. Modul Rate
8. Penggunaan library/packages SweetAlert
9. Mail
10. Raja Ongkir untuk Provinsi dan Kota
11. Model Binding / Resource

-   Iqbal Maulana Muhammad (mochiqbal127@gmail.com) , Pengerjaan:

1. ERD
2. Database Migration
3. Setting Model
4. Templating & UI Dashboard Admin
5. CRUD User
6. CRUD Category
7. CRUD Item
8. Tampil, Tambah, Edit Profile pada Dashboard Admin
9. Pemasangan & Penggunaan library/packages DataTables
10. Pemasangan & Penggunaan library/packages Select2
11. Pemasangan library/packages SweetAlert
12. Middleware isAdmin & isActive
13. Setting Account untuk Customer
14. Delete Account dengan Modal pada Customer
15. Feedback & Alert
16. Delete Modal pada Rate

-   Panji Eka Prasetyo (panjieka8@gmail.com) ,
    Pengerjaan :

1. Tambah Profile Customer
2. Tampil Profile Customer
3. Edit Profile Customer
4. Page Statis Our Team pada Home
5. UI Home

# Tema Project

E - Commerce

# ERD

![gambar erd](/public/img/erd-final-project.png)

# Link

-   Link Video : [GDrive](https://drive.google.com/drive/folders/1YIev-wejLa_w1Sz41Rn3Th5N-vegWIQ0?usp=sharing)
-   Link Website : [JCC-Shop](http://sheltered-plains-97241.herokuapp.com/)
