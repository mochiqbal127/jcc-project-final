<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('item_id');
            $table->integer('user_id');
            $table->integer('jumlah_barang')->default(1);
            $table->string('status_pembayaran')->default('Belum Bayar');
            $table->string('status_barang')->default('Ditambahkan ke cart');
            $table->string('pembayaran')->nullable();
            $table->double('ongkir')->nullable();
            $table->double('harga_barang')->nullable();
            $table->double('total')->nullable();
            $table->string('ekspedisi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
