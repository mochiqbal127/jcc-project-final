<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
      <div class="sidebar-brand-text">JCC Final Project</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    @if(auth()->user()->isAdmin == 1)
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
      <a class="nav-link" href="/dashboard">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Data Management
    </div>
    <li class="nav-item">
      <a class="nav-link" href="/dashboard/category">
        <i class="fas fa-fw fa-table"></i>
        <span>Category</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/dashboard/item">
        <i class="fas fa-fw fa-cookie"></i>
        <span>Item</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/dashboard/user">
        <i class="fas fa-fw fa-user"></i>
        <span>User</span></a>
    </li>
    @else
    {{-- <li class="nav-item">
        <a class="nav-link" href="/profile">
        <i class="fas fa-fw fa-book"></i>
        <span>Akun</span></a>
    </li> --}}
    <li class="nav-item">
        <a class="nav-link" href="/profile">
        <i class="fas fa-fw fa-book"></i>
        <span>Profil</span></a>
    </li>
	@endif

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <div class="sidebar-heading">
        Transaction
    </div>
    <li class="nav-item">
        <a class="nav-link" href="/dashboard/order">
        <i class="fas fa-fw fa-shopping-cart"></i>
        <span>Order</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <div class="sidebar-heading">
        Reviews
    </div>
    <li class="nav-item">
        <a class="nav-link" href="/dashboard/rate">
        <i class="fas fa-fw fa-book"></i>
        <span>Rate</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>

