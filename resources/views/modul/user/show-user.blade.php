@extends('master')
@section('title')
	Detail User
@endsection
@section('content')
{{-- @include('part.feedback') --}}
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Detail User ID {{ $user->id }}</h6>
        <div class="d-inline-block">
            <a href="{{ url('dashboard/user') }}" class="btn btn-sm btn-danger shadow-sm"><i class="fa fa-undo"></i> Back</a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-stripped" style="overflow: auto;">
                <tbody>
                    <tr>
                        <td colspan="3"><h3>Akun :</h3></td>
                    </tr>
                    <tr>
                        <td>Username </td>
                        <td> :</td>
                        <td> {{$user->name}}</td>
                    </tr>
                    <tr>
                        <td>Email </td>
                        <td> :</td>
                        <td> {{$user->email}}</td>
                    </tr>
                    <tr>
                        <td>Role</td>
                        <td> :</td>
                        <td> {{$user->role_display}}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td> :</td>
                        <td> {{$user->status_display}}</td>
                    </tr>
                    <tr>
                        <td colspan="3"><h3>Profil :</h3></td>
                    </tr>
                    @if(!empty($profile))
                    <tr>
                        <td>Nama Lengkap</td>
                        <td> :</td>
                        <td> {{$profile->nama_lengkap}}</td>
                    </tr>
                    <tr>
                        <td>Foto</td>
                        <td> :</td>
                        <td>
                            <img src="\img\profile\{{ $profile->foto }}" alt="" width="50%">
                        </td>
                    </tr>
                    <tr>
                        <td>No HP</td>
                        <td> :</td>
                        <td> {{$profile->no_hp}}</td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td> :</td>
                        <td>
                            @foreach($provinces as $provinsi)
                            @if($provinsi['province_id']==$profile->provinsi)
                                {{$provinsi['province']}}
                            @endif
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>Kota</td>
                        <td> :</td>
                        <td>
                            @foreach($cities as $kota)
                            @if($kota['city_id']==$profile->kota)
                              {{$kota['city_name']}}
                            @endif
                          @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>Alamat </td>
                        <td> :</td>
                        <td> {{$profile->alamat}}</td>
                    </tr>
                    @endif
                    @if(empty($profile))
                    <tr>
                        <td colspan="3">
                            <a href="{{ url('dashboard/profile/create/'.$user->id) }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah Profile</a>
                        </td>
                    </tr>
                    @else
                    <tr>
                        <td colspan="3">
                            <a href="{{ url('dashboard/profile/'.$profile->id.'/edit') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-pen"></i> Update Profile</a>
                        </td>
                    </tr>
                    @endif
                </tbody>
              </table>

        </div>
    </div>
</div>
@endsection

@if(session('success'))

  @push('scripts')
  <script>
    {!! session('success') !!}

  </script>

  @endpush
@endif
