@extends('master')
@section('title')
	User
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List User</h6>
        <div class="d-inline-block">
            <a href="{{ url('dashboard/user/create') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover" style="overflow: auto;">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Email</td>
                        <td>Role</td>
                        <td>Status</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $item)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->role_display}}</td>
                        <td>{{$item->status_display}}</td>
                        <td>
                            <form action="/dashboard/user/{{$item->id}}" method="post">
                                <a href="/dashboard/user/{{$item->id}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                <a href="/dashboard/user/{{$item->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                @csrf
                                @method('delete')
                                @if ($item->isActive != 0)
                                <button type="submit" class="btn btn-danger"><i class="fas fa-power-off"></i></button>
                                @else
                                <button type="submit" class="btn btn-success"><i class="fas fa-power-off"></i></button>
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>

        </div>
    </div>
</div>
@endsection
@push('scripts')

@endpush
@if(session('success'))

  @push('scripts')
  <script>
    {!! session('success') !!}

  </script>

  @endpush
@endif
