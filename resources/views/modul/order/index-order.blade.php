@extends('master')
@section('title')
	Order
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List Order</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover" style="overflow: auto;">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Nama Pemesan</td>
                        <td>Jumlah Barang</td>
                        <td>Harga Total</td>
                        <td>Link Pembayaran</td>
                        <td>Alamat</td>
                        <td>Kota</td>
                        <td>Provinsi</td>
                        <td>Status Pesanan</td>
                        <td>Status Pembayaran</td>
                        <td>Tanggal Pemesanan</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$order->item->nama_barang}}</td>
                        <td>{{$order->user->profile->nama_lengkap}}</td>
                        <td>{{$order->jumlah_barang}}</td>
                        <td>{{$order->total}}</td>
                        <td>{{$order->pembayaran}}</td>
                        <td>{{$order->user->profile->alamat}}</td>
                        <td>
                        	@foreach(App\Http\Controllers\OngkirController::city($order->user->profile->provinsi) as $provinsi)
                        		@if($provinsi['city_id'] == $order->user->profile->kota)
                        			{{$provinsi['city_name']}}
                        		@endif
                        	@endforeach
                        </td>
                        <td>
                        	{{App\Http\Controllers\OngkirController::city($order->user->profile->provinsi)[0]['province']}}
                        </td>
                        <td>{{$order->status_barang}}</td>
                        <td>{{$order->status_pembayaran}}</td>
                        <td>{{$order->created_at}}</td>
                        <td>
                        	<!-- Button trigger modal -->
													<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editOrder{{$order->id}}">
													  <i class="fas fa-edit"></i>
													</button>

													<!-- Modal -->
													<div class="modal fade" id="editOrder{{$order->id}}" tabindex="-1" aria-labelledby="label{{$order->id}}" aria-hidden="true">
													  <div class="modal-dialog">
													    <div class="modal-content">
													      <div class="modal-header">
													        <h5 class="modal-title" id="label{{$order->id}}">Edit Order</h5>
													        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													          <span aria-hidden="true">&times;</span>
													        </button>
													      </div>
													      <form action="/dashboard/order/{{$order->id}}" method="post">
													      	@csrf
														      <div class="modal-body">
														        <div class="form-group">
														        	<label for="pembayaran">Link Pembayaran</label>
														        	<input type="text" name="pembayaran" id="pembayaran" class="form-control" value="{{$order->pembayaran}}">
														        </div>
														        <div class="form-group">
														        	<label for="status_pembayaran">Status Pembayaran</label>
														        	<select name="status_pembayaran" id="status_pembayaran" class="form-control">
														        		<option @if($order->status_pembayaran == "Belum Bayar") selected @endif>Belum Bayar</option>
														        		<option @if($order->status_pembayaran == "Sudah Bayar") selected @endif>Sudah Bayar</option>
														        	</select>
														        </div>
														      </div>
														      <div class="modal-footer">
														        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
														        @method('put')
														        <button type="submit" class="btn btn-primary">Save changes</button>
														      </div>
													      </form>
													    </div>
													  </div>
													</div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>

        </div>
    </div>
</div>
@endsection
@if(session('success'))

  @push('scripts')
  <script>
    {!! session('success') !!}

  </script>

  @endpush
@endif