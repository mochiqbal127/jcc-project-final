@extends('master')
@section('title')
	Detail Item
@endsection
@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Detail Barang ID {{ $item->id }}</h6>
        <div class="d-inline-block">
            <a href="{{ url('dashboard/item') }}" class="btn btn-sm btn-danger shadow-sm"><i class="fa fa-undo"></i> Back</a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover" style="overflow: auto;">
                <tbody>
                    <tr>
                        <td>Nama Barang</td>
                        <td> :</td>
                        <td> {{$item->nama_barang}}</td>
                    </tr>
                    <tr>
                        <td>Foto</td>
                        <td> :</td>
                        <td> <img src="{{ asset('/img/item/'.$item->foto) }}" style="width: 300px;"></td>
                    </tr>
                    <tr>
                        <td>Harga</td>
                        <td> :</td>
                        <td> {{$item->harga}}</td>
                    </tr>
                    <tr>
                        <td>Deskripsi</td>
                        <td> :</td>
                        <td> {{$item->deskripsi}}</td>
                    </tr>
                    <tr>
                        <td>Stok</td>
                        <td> :</td>
                        <td> {{$item->stok}}</td>
                    </tr>
                    <tr>
                        <td>Kategori</td>
                        <td> :</td>
                        <td> {{$item->category->nama}}</td>
                    </tr>
                    {{-- <tr>
                        <td>Jenis Barang</td>
                        <td> :</td>
                        <td> {{$item->jenis_barang}}</td>
                    </tr> --}}

                </tbody>
              </table>

        </div>
    </div>
</div>
@endsection
