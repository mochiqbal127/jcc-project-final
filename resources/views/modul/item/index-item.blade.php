@extends('master')
@section('title')
	Item
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List Barang</h6>
        <div class="d-inline-block">
            <a href="{{ url('dashboard/item/create') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover" style="overflow: auto;">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        {{-- <td>Foto</td> --}}
                        <td>Harga</td>
                        <td>Stok</td>
                        <td>Kategori</td>
                        {{-- <td>Jenis Barang</td> --}}
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$item->nama_barang}}</td>
                        {{-- <td><img src="/img/item/{{$item->foto}}" style="width: 200px"></td> --}}
                        <td>Rp. {{$item->harga}}</td>
                        <td>{{$item->stok}}</td>
                        <td>{{$item->category->nama}}</td>
                        {{-- <td>{{$item->jenis_barang}}</td> --}}
                        <td>
                            <form action="/dashboard/item/{{$item->id}}" method="post">
                                <a href="/dashboard/item/{{$item->id}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                <a href="/dashboard/item/{{$item->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>

        </div>
    </div>
</div>
@endsection
@if(session('success'))

  @push('scripts')
  <script>
    {!! session('success') !!}

  </script>

  @endpush
@endif
