@extends('master')
@section('title')
	Tambah Barang
@endsection
@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Tambah Barang</h6>
        <div class="d-inline-block">
            <a href="{{ url('dashboard/item') }}" class="btn btn-sm btn-danger shadow-sm"><i class="fa fa-undo"></i> Back</a>
        </div>
    </div>
    <div class="card-body">
        <div class="form">
            <form action="/dashboard/item" method="post" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="nama_barang">Nama Barang :</label>
					<input type="text" name="nama_barang" id="nama_barang" class="form-control">
				</div>
				<div class="form-group">
					<label for="foto">Gambar Barang</label>
					<input type="file" name="foto" id="foto" class="form-control">
				</div>
				<div class="form-group">
					<label for="harga">Harga Barang</label>
					<input type="number" name="harga" id="harga" class="form-control">
				</div>
				<div class="form-group">
					<label for="deskripsi">Deskripsi Barang</label>
					<textarea name="deskripsi" id="deskripsi" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label for="stok">Stok Barang</label>
					<input type="number" name="stok" id="stok" class="form-control">
				</div>
				<div class="form-group">
					<label for="category_id">Kategori</label>
					<select name="category_id" id="category_id" class="form-control select2">
						<option> -- Pilih Kategori --</option>
						@foreach($categories as $category)
							<option value="{{$category->id}}">{{$category->nama}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i>Simpan</button>
				</div>
			</form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
    $('.select2').select2();
});
</script>
@endpush

@push('style')

@endpush
