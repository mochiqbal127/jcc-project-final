@extends('home.master')
@section('content')
<div class="container">
	
	<h1>
		Beri Nilai Untuk : {{$item->nama_barang}}
	</h1>
	<hr>
	<form action="" method="post">
		@csrf
		<input type="hidden" name="id" value="{{$item->order->id}}">
		<div class="row">
			<div class="col-md-3">
				<label for="nilai">Nilai : </label>	
			</div>
			<div class="col-md-3">
				<input type="number" name="nilai" id="nilai" class="form-control" min="1" max="5">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-3">
				<label for="isi">Komentar :</label>
			</div>
			<div class="col-md-6">
				<textarea class="form-control" id="isi" name="isi"></textarea>
			</div>
		</div>
		<button class="btn btn-primary" type="submit">Simpan</button>
	</form>
</div><br>
@endsection