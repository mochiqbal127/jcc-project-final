@extends('master')
@section('title')
    Rate
@endsection
@section('content')
<div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger">
          <h5 class="modal-title text-white" id="exampleModalLabel">Delete Modal</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p>Yakin mau menghapus komentar ini ?</p>
            <b>Komentar yang dihapus akan selamanya hilang.</b>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <form action="/profile/" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </div>
      </div>
    </div>
</div>
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List Order</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover" style="overflow: auto;">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Nama Customer</td>
                        <td>Email</td>
                        <td>Nilai</td>
                        <td>Isi Komentar</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rates as $rate)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$rate->item->nama_barang}}</td>
                        <td>{{$rate->user->profile->nama_lengkap}}</td>
                        <td>{{$rate->user->email}}</td>
                        <td>{{$rate->nilai}}</td>
                        <td>{{$rate->isi}}</td>
                        <td>
                            <button type="button" data-toggle="modal" data-target="#DeleteRateModal{{ $rate->id }}" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                            <div class="modal fade" id="DeleteRateModal{{ $rate->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                      <h5 class="modal-title text-white" id="exampleModalLabel">Delete Modal</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Yakin mau menghapus komentar ini ?</p>
                                        <b>Komentar yang dihapus akan selamanya hilang dan customer bisa menambah komentar baru.</b>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <form action="/dashboard/rate/{{ $rate->id }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>

        </div>
    </div>
</div>
@endsection
@if(session('success'))

  @push('scripts')
  <script>
    {!! session('success') !!}

  </script>

  @endpush
@endif
