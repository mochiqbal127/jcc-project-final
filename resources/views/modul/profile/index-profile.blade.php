<!-- Start Page Content -->
<!-- ============================================================== -->

@extends('master')

@section('title')
    <title>Profile Pages</title>
@endsection

@section('content')
@if(!auth()->user()->profile)
<a href="{{ url('dashboard/profile/create') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
@endif
    <!-- Column -->
  <div class="card">
    <div class="card-body">
      <div class="m-t-30">
        <div class="row gutters-sm">
          <div class="col-md-4 mb-3">
            <div class="card">
              <div class="card-body">
                <div class="d-flex flex-column align-items-center text-center">
                  <img src="{{ asset('/img/profile/'.auth()->user()->profile->foto) }}" alt="Admin" class="rounded-circle" width="150">
                </div>
              </div>
            </div>
            <div class="card mt-3 align-items-center">
              <h4>{{auth()->user()->name}}</h4>
              <h5></h5>
            </div>
          </div>
          <div class="col-md-8">
            <div class="card mb-3">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3">
                    <h6 class="mb-0">Nama Lengkap</h6>
                  </div>
                  <div class="col-sm-9 text-secondary">
                    {{auth()->user()->profile->nama_lengkap}}
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-sm-3">
                    <h6 class="mb-0">Email</h6>
                  </div>
                  <div class="col-sm-9 text-secondary">
                      {{auth()->user()->email}}
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-sm-3">
                    <h6 class="mb-0">No Telepon</h6>
                  </div>
                  <div class="col-sm-9 text-secondary">
                      {{auth()->user()->profile->no_hp}}
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-sm-3">
                    <h6 class="mb-0">Provinsi</h6>
                  </div>
                  <div class="col-sm-9 text-secondary">
                    @foreach($provinces as $provinsi)
                      @if($provinsi['province_id']==auth()->user()->profile->provinsi)
                        {{$provinsi['province']}}
                      @endif
                    @endforeach
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-sm-3">
                    <h6 class="mb-0">Kota</h6>
                  </div>
                  <div class="col-sm-9 text-secondary">
                    @foreach($cities as $kota)
                      @if($kota['city_id']==auth()->user()->profile->kota)
                        {{$kota['city_name']}}
                      @endif
                    @endforeach
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-sm-3">
                    <h6 class="mb-0">Alamat</h6>
                  </div>
                  <div class="col-sm-9 text-secondary">
                      {{auth()->user()->profile->alamat}}
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-sm-12">
                    <a class="btn btn-info "  href="/dashboard/profile/{{auth()->user()->profile->id}}/edit">Edit</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

              <!-- ============================================================== -->
              <!-- End PAge Content -->
              <!-- ============================================================== -->
@endsection
