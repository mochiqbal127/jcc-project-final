@extends('master')

@section('title')

@endsection

@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Edit Kategori</h6>
        <div class="d-inline-block">
            <a href="{{ url('dashboard/category/') }}" class="btn btn-sm btn-danger shadow-sm"><i class="fa fa-undo"></i> Back</a>
        </div>
    </div>
    <div class="card-body">
        <div class="form">
            <form data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/dashboard/category/'.$category->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama<span class="required">*</span></label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" value="{{ $category->nama }}" id="nama" name="nama" required="required" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn btn-sm btn-success shadow-sm">Edit</button>
                <div class="ln_solid"></div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

</script>
@endpush
