@extends('master')

@section('title')
    Category
@endsection

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List Category</h6>
        <div class="d-inline-block">
            {{-- <a href="{{ url('dashboard/category/create') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a> --}}
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#tambahData">
              <i class="fa fa-plus"></i> Tambah
            </button>
        </div>
    </div>
    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal fade" id="tambahData" tabindex="-1" aria-labelledby="tambahDataLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="tambahDataLabel">Tambah Kategori</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/dashboard/category" method="post">
            @csrf
              <div class="modal-body">
                <div class="form-group">
                    <label for="nama">Nama Kategori</label>
                    <input type="text" name="nama" id="nama" class="form-control">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" id="dtcate" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($category as $item)
                <tr>
                    <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>
                        {{-- <a href="{{ url("/category/$item->id") }}" class="btn btn-sm btn-info"><i class='fa fa-eye px-1'></i></a> --}}
                        {{-- <a href="{{ url("/dashboard/category/$item->id/edit") }}" class="btn btn-sm btn-primary"><i class='fa fa-pen px-1'></i></a> --}}
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editData{{$item->id}}">
                          <i class='fa fa-edit px-1'></i>
                        </button>
                        <form action="{{ url("/dashboard/category/$item->id") }}" method="POST" class="d-sm-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"><i class='fa fa-trash px-1'></i></button>
                        </form>
                    </td>
                </tr>

                <div class="modal fade" id="editData{{$item->id}}" tabindex="-1" aria-labelledby="editDataLabel{{$item->id}}" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="editDataLabel{{$item->id}}">Edit Kategori</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form action="/dashboard/category/{{$item->id}}" method="post">
                        @csrf
                        @method('put')
                          <div class="modal-body">
                            <div class="form-group">
                                <label for="nama">Nama Kategori</label>
                                <input type="text" name="nama" id="nama" class="form-control" value="{{$item->nama}}">
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>



                @endforeach
            </tbody>
        </table>

        </div>
    </div>
</div>
@endsection
@if(session('success'))

  @push('scripts')
  <script>
    {!! session('success') !!}

  </script>

  @endpush
@endif
