		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
					</ul>
					<ul class="header-links pull-right">
						@auth
							@if(auth()->user()->isAdmin == 1)
                            <li><a href="/dashboard"><i class="fa fa-user-o"></i> Dashboard</a></li>
							<li><a href="/dashboard/user/{{ auth()->user()->id }}"><i class="fa fa-user-o"></i> My Account</a></li>
							@else
                            @if(auth()->user()->profile != null)
                            <li><a href="/profile"><i class="fa fa-user-o"></i> My Account</a></li>
                            @else
							<li><a href="/profile/create"><i class="fa fa-user-o"></i> My Account</a></li>
                            @endif
                            @endif
							<li><a href="/cart"><i class="fa fa-shopping-cart"></i> My Order</a></li>
							<li><a href="/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
						@else
							<li><a href="/signin"><i class="fa fa-sign-in"></i> Login</a></li>
						@endauth
						{{-- <li>
							<div class="menu-toggle">
								<a href="#">
									<i class="fa fa-bars"></i>
									<span>Menu</span>
								</a>
							</div>
						</li> --}}
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-6">
							<div class="header-logo" style="height: 80px; overflow: hidden;">
									<img src="/img/logoshop.png" alt="" style="margin-top: -65px;">
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form action="/" method="post">
									@csrf
									<input class="input" placeholder="Search here" name="key">
									<button class="search-btn" type="submit">Search</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
