@extends('master')
@section('title')
	Account Setting
@endsection
@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Account Setting</h6>
        <div class="d-inline-block">
            <a href="{{ url('/profile') }}" class="btn btn-sm btn-danger shadow-sm"><i class="fa fa-undo"></i> Back</a>
        </div>
    </div>
    <div class="card-body">
        <div class="form">
            <form action="/akun" method="post">
				@csrf
                @method('put')
				<div class="form-group">
					<label for="name">Username :</label>
					<input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
				</div>
				<div class="form-group">
					<label for="email">Email :</label>
					<input type="text" name="email" id="email" class="form-control" value="{{ $user->email }}">
				</div>
				<div class="form-group">
					<label for="password">New Password :</label>
					<input type="password" name="password" id="password" class="form-control">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Save</button>
					<button type="button" data-toggle="modal" data-target="#DeleteModal" class="btn btn-danger"><i class="fas fa-trash"></i> Delete Account</button>
				</div>
			</form>
        </div>
        <div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header bg-danger">
                  <h5 class="modal-title text-white" id="exampleModalLabel">Delete Modal</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <p>Yakin mau menghapus akun ? akun yang dihapus hanya akan dinonaktifkan.</p>
                    <b>Untuk mengaktifkan kembali akun yang sudah nonaktif, harus menghubungi admin JCC-Shop</b>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form action="/profile/{{ auth()->user()->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush

@push('style')

@endpush
