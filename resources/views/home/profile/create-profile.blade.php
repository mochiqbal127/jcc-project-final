@extends('master')
@section('title')
    Add Profile
@endsection
@section('content')

<div class="container bootstrap snippets bootdey">
    <h1 class="text-primary"><span class="glyphicon glyphicon-user"></span>Tambah Profile</h1>
      <hr>
	<div class="row">
      <!-- Tambah form column -->
      <div class="col-md-9 personal-info">

        <h3>Tambah Profile</h3>

        <form action="/profile" method="post" enctype="multipart/form-data">
            @csrf
            <div class="col-lg-8">
                <div class="text-center">
                  <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
                  <h6>Upload photo...</h6>
                  <input type="file" name="foto" id="foto" class="form-control">
                </div>
              </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Nama Lengkap:</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text" id="nama_lengkap" name="nama_lengkap" >
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-3 control-label">No. Handphone:</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text" id="no_hp" name="no_hp">
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-3 control-label">Provinsi:</label>
                <div class="col-lg-8">
                  <select name="provinsi" id="provinsi" class="form-control select2">
                    <option value="">-- Pilih Provinsi --</option>
                    @foreach ($provinces  as $provinsi)
                      <option value="{{$provinsi['province_id']}}" @if($provinsi['province_id']==auth()->user()->provinsi)selected @endif>{{$provinsi['province']}}</option>
                    @endforeach
                </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-3 control-label">Kota:</label>
                <div class="col-lg-8">
                  <select name="kota" id="kota" class="form-control select2"></select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-3 control-label">Alamat:</label>
                <div class="col-lg-8">
                  <textarea class="form-control"  id="alamat" name="alamat"></textarea>
                </div>
              </div>
              <input class="form-control" type="hidden" id="user_id" name="user_id" value="{{auth()->user()->id}}">
              <button type="submit" class="btn btn-sm btn-success shadow-sm">Save</button>
                <div class="ln_solid"></div>
        </form>
      </div>
  </div>
</div>
<hr>

@endsection

<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script>

  $(document).ready(function(){
  //ini ketika provinsi tujuan di klik maka akan eksekusi perintah yg kita mau
  //name select nama nya "provinve_id" kalian bisa sesuaikan dengan form select kalian
    $('select[name="provinsi"]').on('change', function(){
    // kita buat variable provincedid untk menampung data id select province
      let provinceid = $(this).val();
      //kita cek jika id di dpatkan maka apa yg akan kita eksekusi
      if(provinceid){
      // jika di temukan id nya kita buat eksekusi ajax GET
        jQuery.ajax({
        // url yg di root yang kita buat tadi
          url:"/get_city/"+provinceid,
          // aksion GET, karena kita mau mengambil data
          type:'GET',
          // type data json
          dataType:'json',
          // jika data berhasil di dapat maka kita mau apain nih
          success:function(data){
          // jika tidak ada select dr provinsi maka select kota kososng / empty
            $('select[name="kota"]').empty();
            // jika ada kita looping dengan each
            $.each(data, function(key, value){
            // perhtikan dimana kita akan menampilkan data select nya, di sini saya memberi name select kota adalah kota_id
            $('select[name="kota"]').append('<option value="'+ value.city_id +'" namakota="'+ value.type +' ' +value.city_name+ '">' + value.type + ' ' + value.city_name + '</option>');
            });
          }
        });
      }else {
      $('select[name="kota"]').empty();
      }
    });
  });

</script>
