@extends('home.master')
@section('content')

<div class="container">
	<div class="row">
	@foreach($items as $item)
	<?php
		$reviews = count($item->rates);
		$nilaitotal = 0;
		foreach ($item->rates as $rate) {
			$nilaitotal += $rate->nilai;
		}
		if($reviews>0){
			$mean = round($nilaitotal/$reviews);
		}else{
			$mean = 0;
		}
		$minus = 5-$mean;
	;?>
		<div class="col-md-3">
				<!-- product -->
				<div class="product">
					<div class="product-img">
						<img src="/img/item/{{$item->foto}}" alt="">
						<div class="product-label">
							<span class="sale">-10%</span>
						</div>
					</div>
					<div class="product-body">
						<p class="product-category">{{$item->category->nama}}</p>
						<h3 class="product-name"><a href="#">{{$item->nama_barang}}</a></h3>
						<h4 class="product-price">Rp {{$item->harga}}<del class="product-old-price">Rp {{$item->harga + $item->harga * 0.1}}</del></h4>
						<div class="product-rating">
							@for($i=0;$i<$mean;$i++)
								<i class="fa fa-star"></i>
							@endfor
							@for($i=0;$i<$minus;$i++)
								<i class="fa fa-star-o"></i>
							@endfor
						</div>
							<p>{{$reviews}} Review</p>
						<div class="product-btns">
							<a class="quick-view" href="/item/{{$item->slug}}"><i class="fa fa-eye"></i><span class="tooltipp"> Lihat</span></a>
						</div>
					</div>
				</div>
				<!-- /product -->
		</div>
	@endforeach
	@if(count($items)<1)
	<p>Barang Tidak DItemukan</p>
	@endif

	</div>

</div>



@endsection
