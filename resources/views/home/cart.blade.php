@extends('home.master')
@section('content')

<div class="container">
	<div class="row">
		
					<div class="col-md-12">
						<div id="product-tab">
							<!-- product tab nav -->
							<ul class="tab-nav">
								<li class="active"><a data-toggle="tab" href="#tab1">Keranjang</a></li>
								<li><a data-toggle="tab" href="#tab2">Dipesan</a></li>
								<li><a data-toggle="tab" href="#tab3">Selesai</a></li>
							</ul>
							<!-- /product tab nav -->

							<!-- product tab content -->
							<div class="tab-content">
								<div id="tab1" class="tab-pane fade in active">
									<div class="row">
										@foreach($orders as $order)
											@if($order->status_barang == "Ditambahkan ke cart")
											<div class="col-md-6">
											<hr>
												<div class="row">
													<div class="col-md-5" style="border-right: gray 1px solid">
														<img src="/img/item/{{$order->item->foto}}" width="200px">
													</div>
													<div class="col-md-7">
														<h2>{{$order->item->nama_barang}}</h2>
														<hr>
														<p>Rp {{$order->item->harga}}</p>
														<div class="row">
															<div class="col-sm-4">
																<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#checkout{{$order->id}}">
																  Check Out
																</button>

																<!-- Modal -->
																<div class="modal fade" id="checkout{{$order->id}}" tabindex="-1" aria-labelledby="label{{$order->id}}" aria-hidden="true">
																  <div class="modal-dialog">
																    <div class="modal-content">
																      <div class="modal-header">
																        <h5 class="modal-title" id="label{{$order->id}}">Jumlah Barang</h5>
																        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
																          <span aria-hidden="true">&times;</span>
																        </button>
																      </div>
																			<form action="/checkout/order_id/{{$order->id}}" method="post">
																				@csrf
																	      <div class="modal-body">
																	        <input type="number" name="jumlah_barang" class="form-control" value="1">
																	      </div>
																	      <div class="modal-footer">
																	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																	        <button type="submit" class="btn btn-primary">Checkout</button>
																	      </div>
																			</form>
																    </div>
																  </div>
																</div>
															</div>
															<div class="col-sm-4">
																<form action="/cart/{{$order->id}}" method="post">
																	@csrf
																	@method('delete')
																	<button class="btn btn-danger" type="submit">Hapus</button>
																</form>
															</div>
															<div class="col-sm-4">
															</div>
														</div>
													</div>
												</div>
											<hr>
											<br>
												
											</div>
											@endif
										@endforeach
									</div>
								</div>
								<div id="tab2" class="tab-pane fade in">
									<div class="row">
										@foreach($orders as $order)
											@if($order->status_barang == "dipesan")
											<div class="col-md-6">
											<hr>
												<div class="row">
													<div class="col-md-5" style="border-right: gray 1px solid">
														<img src="/img/item/{{$order->item->foto}}" width="200px">
													</div>
													<div class="col-md-7">
														<h2>{{$order->item->nama_barang}}</h2>
														<hr>
														<p>Rp {{$order->total}}</p>
														<div class="row">
															<div class="col-sm-4">
																{{--<a href="/bayar/order_id/{{$order->id}}" class="btn btn-primary">Bayar</a>--}}
																@if($order->pembayaran)
																<a href="{{$order->pembayaran}}" class="btn btn-primary">Bayar</a>
																@else
																<p>Link Dibuat</p>
																@endif
															</div>
															<div class="col-sm-4">
															</div>
															<div class="col-sm-4">
															</div>
														</div>
													</div>
												</div>
											<hr>
											<br>
												
											</div>
											@endif
										@endforeach
									</div>
								</div>
								<div id="tab3" class="tab-pane fade in">
									<div class="row">
										@foreach($orders as $order)
											@if($order->status_barang == "selesai")
											<div class="col-md-6">
											<hr>
												<div class="row">
													<div class="col-md-5" style="border-right: gray 1px solid">
														<img src="/img/item/{{$order->item->foto}}" width="200px">
													</div>
													<div class="col-md-7">
														<h2>{{$order->item->nama_barang}}</h2>
														<hr>
														<p>Rp {{$order->total}}</p>
														<div class="row">
															<div class="col-sm-4">
																@if(!$order->rate)
																<a href="/rate/create/item_{{$order->item->id}}" class="btn btn-primary">Beri Nilai</a>
																@else
																<p>Nilai : {{$order->rate->nilai}}/5</p>
																<p><i>"{{$order->rate->isi}}"</i></p>
																@endif
															</div>
															<div class="col-sm-4">
															</div>
															<div class="col-sm-4">
															</div>
														</div>
													</div>
												</div>
											<hr>
											<br>
												
											</div>
											@endif
										@endforeach
									</div>
								</div>
							</div>
							<!-- /product tab content  -->
						</div>
					</div>

	</div>
	
</div>



@endsection