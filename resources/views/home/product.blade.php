@extends('home.master')
@section('content')
	<?php
		$reviews = count($item->rates);
		$nilaitotal = 0;
		foreach ($item->rates as $rate) {
			$nilaitotal += $rate->nilai;
		}
		if($reviews>0){
			$mean = round($nilaitotal/$reviews);
		}else{
			$mean = 0;
		}

		$minus = 5-$mean;
	;?>
		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- Product main img -->
					<div class="col-md-5">
						<div id="product-main-img">
							<div class="product-preview">
								<img src="/img/item/{{$item->foto}}" alt="">
							</div>
						</div>
					</div>
					<!-- /Product main img -->


					<!-- Product details -->
					<div class="col-md-5">
						<div class="product-details">
							<h2 class="product-name">{{$item->nama_barang}}</h2>
							<div>
								<div class="product-rating">
									@for($i=0;$i<$mean;$i++)
										<i class="fa fa-star"></i>
									@endfor
									@for($i=0;$i<$minus;$i++)
										<i class="fa fa-star-o"></i>
									@endfor
								</div>
								<p class="review-link" href="#">{{$reviews}} Review(s)</p>
							</div>
							<div>
								<h3 class="product-price">Rp {{$item->harga}}<del class="product-old-price">Rp {{$item->harga + $item->harga * 0.1}}</del></h3>
								@if($item->stok>0)
								<span class="product-available">In Stock</span>
								@else
								<span class="product-available">Out Stock</span>
								@endif
							</div>
							<div class="add-to-cart">
								<form action="/cart" method="post">
									@csrf
									<input type="hidden" name="item_id" value="{{$item->id}}">
									<button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
								</form>
							</div>
							<ul class="product-links">
								<li>Category:</li>
								<li><a href="/category/{{$item->category->nama}}">{{$item->category->nama}}</a></li>
							</ul>


						</div>
					</div>
					<!-- /Product details -->

					<!-- Product tab -->
					<div class="col-md-12">
						<div id="product-tab">
							<!-- product tab nav -->
							<ul class="tab-nav">
								<li class="active"><a data-toggle="tab" href="#tab1">Description</a></li>
								<li><a data-toggle="tab" href="#tab2">Reviews</a></li>
							</ul>
							<!-- /product tab nav -->

							<!-- product tab content -->
							<div class="tab-content">
								<div id="tab1" class="tab-pane fade in active">
									<div class="row">
										<div class="col-md-12">
											<p>{{$item->deskripsi}}</p>
										</div>
									</div>
								</div>
								<div id="tab2" class="tab-pane fade in">
									<div class="row">
										<div class="col-md-3"></div>

										<!-- Reviews -->
										<div class="col-md-6">
											<div id="reviews">
												<ul class="reviews">
													@foreach($rates as $rate)
													<li>
														<div class="review-heading">
															<h5 class="name">{{$rate->user->profile->nama_lengkap}}</h5>
															<p class="date">{{$rate->created_at}}</p>
															<div class="review-rating">
																@for($i=0;$i<$rate->nilai;$i++)
																	<i class="fa fa-star"></i>
																@endfor
															</div>
														</div>
														<div class="review-body">
															<p>{{$rate->isi}}</p>
														</div>
													</li>
													@endforeach
												</ul>
												<ul class="reviews-pagination">
													@if($rates->hasPages())
														<li>
															<a href="{{$rates->previousPageUrl()}}"><i class="fa fa-angle-left"></i></a>
														</li>
														@for($i=1;$i<=$rates->total();$i++)
															<li class="@if($rates->currentPage()==$i) active @endif"><a href="{{$rates->url($i)}}">{{$i}}</a></li>
														@endfor
														<li>
															<a href="{{$rates->nextPageUrl()}}"><i class="fa fa-angle-right"></i></a>
														</li>
													@endif
												</ul>
											</div>
										</div>
										<!-- /Reviews -->

									</div>
								</div>
							</div>
							<!-- /product tab content  -->
						</div>
					</div>
					<!-- /product tab -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- Section -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<div class="col-md-12">
						<div class="section-title text-center">
							<h3 class="title">Related Products</h3>
						</div>
					</div>
					@foreach($items as $item)
					<!-- product -->
					<div class="col-md-3 col-xs-6">
						<div class="product">
							<div class="product-img">
								<img src="/img/item/{{$item->foto}}" alt="">
								<div class="product-label">
									<span class="sale">-10%</span>
								</div>
							</div>
							<div class="product-body">
								<p class="product-category">{{$item->category->nama}}</p>
								<h3 class="product-name"><a href="#">{{$item->nama_barang}}</a></h3>
								<h4 class="product-price">Rp {{$item->harga}} <del class="product-old-price">Rp {{$item->harga + $item->harga * 0.1}} </del></h4>
								<div class="product-rating">
								</div>
								<div class="product-btns">
									<a class="quick-view" href="/item/{{$item->nama_barang}}"><i class="fa fa-eye"></i><span class="tooltipp"> Lihat</span></a>
								</div>
							</div>
						</div>
					</div>
					<!-- /product -->
					@endforeach

				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /Section -->


@endsection