@extends('home.master')
@section('content')
	
		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-3"></div>

					<!-- Order Details -->
					<div class="col-md-6 order-details">
						<div class="section-title text-center">
							<h3 class="title">Your Order</h3>
						</div>
						<div class="order-summary">
							<div class="order-col">
								<div><strong>PRODUCT</strong></div>
								<div><strong>TOTAL</strong></div>
							</div>
							<div class="order-products">
								<div class="order-col">
									<div>{{$order->jumlah_barang}}x {{$order->item->nama_barang}}</div>
									<div>{{$order->jumlah_barang * $order->item->harga}}</div>
								</div>
							</div>
							<div class="order-col">
								<div>Ongkos Kirim</div>
								<div>20000</div>
							</div>
							<div class="order-col">
								<div><strong>TOTAL</strong></div>
								<div><strong class="order-total">{{$order->jumlah_barang * $order->item->harga + 20000}}</strong></div>
							</div>
						</div>
						<form action="" method="post">
							@csrf
							@method('put')
							<input type="hidden" name="ongkir" value="20000">
							<input type="hidden" name="harga_barang" value="{{$order->item->harga}}">
							<input type="hidden" name="total" value="{{$order->jumlah_barang * $order->item->harga + 20000}}">
							<button type="submit" class="primary-btn order-submit">Check Out</button>
						</form>
					</div>
					<!-- /Order Details -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

@endsection