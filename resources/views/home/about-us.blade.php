@extends('home.master-about')

@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('about.css')}}">
<div class="container">
    <div class="head">
        <h1>Our Team :</h1>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="card p-0">
                <div class="card-image"> <img src="{{asset('img/team/arwani.jpg')}}" alt=""> </div>
                <div class="card-content d-flex flex-column align-items-center">
                    <h4 class="pt-2">Muhammad Arwani Maulana</h4>
                    
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card p-0">
                <div class="card-image"> <img src="{{asset('img/team/iqbal.jpg')}}"> </div>
                <div class="card-content d-flex flex-column align-items-center">
                    <h4 class="pt-2">Iqbal Maulana Muhammad</h4>
                    
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card p-0">
                <div class="card-image"> <img src="{{asset("img/team/panji.jpg")}}" alt=""> </div>
                <div class="card-content d-flex flex-column align-items-center">
                    <h4 class="pt-2">Panji Eka Prasetyo</h4>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
